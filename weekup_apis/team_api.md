# teams

### 获取 teams 信息
  `GET  /api/teams`

参数名 | 是否必需 | 描述
----- | --------| -------

##### 响应
~~~json
{
    "paginate_meta": {
        "current_page": 1,
        "next_page": null,
        "prev_page": null,
        "total_pages": 1,
        "total_count": 4
    },
    "teams": [
        {
            "id": 4,
            "owner_id": 2,
            "name": "Linux小组",
            "description": "懂Linux的都是大神，我懂linux，所以我就是大神",
            "created_at": "2017-08-02T02:00:56.000Z",
            "updated_at": "2017-08-02T02:00:56.000Z"
        }
    ]
}
~~~


### 获取 team 信息
  `GET /api/teams/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
id   |   是     | team id

##### 响应
~~~json
{
    "id": 2,
    "owner_id": 1,
    "name": "Mazeal",
    "description": "Beijing Mazeal Technology Co., Ltd",
    "created_at": "2017-07-28T09:01:35.000Z",
    "updated_at": "2017-07-28T09:01:35.000Z",
    "users": [
        {
            "id": 1,
            "email": null,
            "name": "Jianing Wang",
            "admin": false,
            "upped": true,
            "phone": "17865169805",
            "avatar": "https://weekup.oss-cn-hangzhou.aliyuncs.com/uploads/user/1/636368023651480064.jpg",
            "created_at": "2017-07-26T07:51:04.000Z",
            "updated_at": "2017-07-28T01:32:46.000Z"
        }
    ]
}
~~~

### 修改 team 信息
`PUT  /api/teams/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
name | 是 | team name
description | 是 | team description

### 创建 team
`POST /api/teams`

参数名 | 类型 | 是否必需 | 描述 | 备注
----- | --- | -----| ---- | ----
name | string | 是 | team name | 小组名
description | string | 是 | team description |
owner_id | integer | 是 | user id | 谁创建的这个小组
user_ids | string | false | 为该小组添加的用户的ids | 例如：1,2,3

##### 响应
```json
{
    "id": 4,
    "owner_id": 2,
    "name": "Linux小组",
    "description": "懂Linux的都是大神，我懂linux，所以我就是大神",
    "created_at": "2017-08-02T02:00:56.000Z",
    "updated_at": "2017-08-02T02:00:56.000Z",
    "users": [
        {
            "id": 2,
            "email": null,
            "name": "test-01",
            "admin": false,
            "upped": true,
            "phone": "13465425936",
            "avatar": null,
            "created_at": "2017-07-31T01:56:48.000Z",
            "updated_at": "2017-07-31T01:56:48.000Z"
        }
    ]
}
```

###  删除 team
  `DELETE /api/teams/:id`

参数名 | 是否必需 | 描述
----- | --------| -------


### 上传 team logo
 `POST teams/:id/logo`

参数名 | 类型 | 是否必需 | 描述
----- | ---- | ----| -------
logo | file | 是 | team logo


### 小组所有的 `activities`
`GET /api/teams/:id/activities`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | team id

##### 响应
```json
{
    "paginate_meta": {
        "current_page": 1,
        "next_page": null,
        "prev_page": null,
        "total_pages": 1,
        "total_count": 12
    },
    "activities": [
        {
            "id": 53,
            "user_id": 1,
            "activity_type": "summary",
            "created_at": "2017-08-09T08:08:13.000Z",
            "updated_at": "2017-08-09T08:08:13.000Z",
            "author": {
                "id": 1,
                "email": null,
                "name": "Jianing Wang",
                "admin": false,
                "upped": true,
                "phone": "17865169805",
                "avatar": "https://weekup.oss-cn-hangzhou.aliyuncs.com/uploads/user/1/636368023651480064.jpg",
                "created_at": "2017-07-26T07:51:04.000Z",
                "updated_at": "2017-07-28T01:32:46.000Z"
            },
            "summary": {
                "id": 1,
                "user_id": 1,
                "activity_id": 53,
                "stars": 4.5,
                "content": "my first summary",
                "week": 2,
                "month": 8,
                "year": 2017,
                "created_at": "2017-08-09T08:08:13.000Z",
                "updated_at": "2017-08-09T08:08:13.000Z",
                "user": {
                    "id": 1,
                    "email": null,
                    "name": "Jianing Wang",
                    "avatar": "https://weekup.oss-cn-hangzhou.aliyuncs.com/uploads/user/1/636368023651480064.jpg"
                }
            }
        },
        {
            "id": 43,
            "user_id": 1,
            "activity_type": "checklist",
            "created_at": "2017-08-07T08:51:23.000Z",
            "updated_at": "2017-08-07T08:51:23.000Z",
            "author": {
                "id": 1,
                "email": null,
                "name": "Jianing Wang",
                "admin": false,
                "upped": true,
                "phone": "17865169805",
                "avatar": "https://weekup.oss-cn-hangzhou.aliyuncs.com/uploads/user/1/636368023651480064.jpg",
                "created_at": "2017-07-26T07:51:04.000Z",
                "updated_at": "2017-07-28T01:32:46.000Z"
            },
            "checklist": {
                "id": 2,
                "user_id": 1,
                "activity_id": 43,
                "year": 2017,
                "month": 8,
                "week": 2,
                "created_at": "2017-08-07T08:51:23.000Z",
                "updated_at": "2017-08-07T08:51:23.000Z",
                "items": []
            }
        },
        {
            "id": 40,
            "user_id": 1,
            "activity_type": "post",
            "created_at": "2017-08-02T07:40:44.000Z",
            "updated_at": "2017-08-02T07:40:44.000Z",
            "author": {
                "id": 1,
                "email": null,
                "name": "Jianing Wang",
                "admin": false,
                "upped": true,
                "phone": "17865169805",
                "avatar": "https://weekup.oss-cn-hangzhou.aliyuncs.com/uploads/user/1/636368023651480064.jpg",
                "created_at": "2017-07-26T07:51:04.000Z",
                "updated_at": "2017-07-28T01:32:46.000Z"
            },
            "post": {
                "id": 1,
                "title": "Test post ",
                "content": "bulabulabulabula bulabulabulabula bulabulabulabula bulabulabulabula ",
                "created_at": "2017-08-02T07:40:44.000Z",
                "updated_at": "2017-08-02T07:40:44.000Z",
                "activity_id": 40,
                "user_id": 1,
                "likes_count": 0,
                "stars_count": 0
            }
        },
        {
            "id": 38,
            "user_id": 2,
            "activity_type": "link",
            "created_at": "2017-08-02T02:55:22.000Z",
            "updated_at": "2017-08-02T03:01:09.000Z",
            "author": {
                "id": 2,
                "email": null,
                "name": "test-01",
                "admin": false,
                "upped": true,
                "phone": "13465425936",
                "avatar": null,
                "created_at": "2017-07-31T01:56:48.000Z",
                "updated_at": "2017-07-31T01:56:48.000Z"
            },
            "link": {
                "id": 37,
                "url": "https://github.com/dreamerlxb/banner",
                "user_id": 2,
                "created_at": "2017-08-02T02:55:22.000Z",
                "updated_at": "2017-08-02T03:01:09.000Z",
                "activity_id": 38,
                "title": "这个库不错",
                "img": null,
                "content": null
            }
        }
    ]
}
```


### 获取小组所有的用户
`GET /api/teams/:id/users`

参数名 | 是否必需 | 描述
----- | --------| -------

##### 响应
```json
{

}
```

### 为小组添加用户
`POST /api/teams/:team_id/users`

参数名 | 类型 | 是否必需 | 描述 | 备注
----- | --- |-----| ----- | --
user_ids |string| 是 | 为小组添加用户 | 1,2,3

##### 响应
```json
{

}
```

### 删除小组用户
`DELETE /api/teams/:team_id/users`

参数名 | 类型 | 是否必需 | 描述 | 备注
----- | --- |-----| ----- | --
user_ids |string| 是 | 将小组用户删除 | 1,2,3

##### 响应
```json
{

}
```


### 获取某年某月某周的某小组 _ 用户_ 的 up 排名
`GET /api/teams/:id/ranks`

参数名 | 类型 | 是否必需 | 描述 | 备注
----- | --- | ------| ---- | ---
`year` | integer | 是 | 那一年 |
`month` | integer | 是 | 那一月 |
`week` | integer | false | 第几周 | 如果未给该参数，那么就是获取到该小组这一个月的所有排名

> `week`参数表示该小组用户在本周的`up`排名 

###### 响应
```json
{
    "paginate_meta": {
        "current_page": 1,
        "next_page": null,
        "prev_page": null,
        "total_pages": 1,
        "total_count": 5
    },
    "ups": [
        {
            "id": 4,
            "email": null,
            "name": "test-03",
            "upped": true,
            "phone": "13465425938",
            "up_counts": 3,
            "created_at": "2017-07-31T01:57:38.000Z",
            "updated_at": "2017-07-31T01:57:38.000Z",
            "avatar": null,
            "week": 3
        }
    ]
}
```

### 小组所有的 `summaries`
`GET /api/teams/:id/summaries`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | team id

##### 响应
```json
{

}
```

### 小组所有的 `POSTS`
`GET /api/teams/:id/posts`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | team id

##### 响应
```json
{
    "paginate_meta": {
        "current_page": 1,
        "next_page": null,
        "prev_page": null,
        "total_pages": 1,
        "total_count": 1
    },
    "posts": [
        {
            "id": 1,
            "title": "Test post ",
            "content": "bulabulabulabula bulabulabulabula bulabulabulabula bulabulabulabula ",
            "created_at": "2017-08-02T07:40:44.000Z",
            "updated_at": "2017-08-02T07:40:44.000Z",
            "activity_id": 40,
            "user_id": 1
        }
    ]
}
```

### 小组所有的 `checklists`
`GET /api/teams/:id/checklists`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | team id

##### 响应
```json
{
    "paginate_meta": {
        "current_page": 1,
        "next_page": null,
        "prev_page": null,
        "total_pages": 1,
        "total_count": 7
    },
    "checklists": [
        {
            "id": 2,
            "user_id": 1,
            "activity_id": 43,
            "year": 2017,
            "month": 8,
            "week": 2,
            "created_at": "2017-08-07T08:51:23.000Z",
            "updated_at": "2017-08-07T08:51:23.000Z"
        }
    ]
}
```

### 小组所有的 `attachments`
`GET /api/teams/:id/attachments`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | team id

##### 响应
```json
{

}
```

### 小组所有的 `links`
`GET /api/teams/:id/links`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | team id

##### 响应
```json
{
    "paginate_meta": {
        "current_page": 1,
        "next_page": null,
        "prev_page": null,
        "total_pages": 1,
        "total_count": 1
    },
    "links": [
        {
            "id": 37,
            "title": "这个库不错",
            "url": "https://github.com/dreamerlxb/banner",
            "created_at": "2017-08-02T02:55:22.000Z",
            "updated_at": "2017-08-02T03:01:09.000Z",
            "activity_id": 38,
            "user_id": 2
        }
    ]
}
```

### 小组所有的 `mentions`
`GET /api/teams/:id/mentions`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | team id

##### 响应
```json
{

}
```

### ~~为小组添加用户~~
`POST /api/teams/:id/add`

参数名 | 是否必需 | 描述 | 备注
----- | ----- | ---- | ---
user_ids | 是 | 用户 id | 例如: 1,2,3,4

> 建议使用 `POST /api/teams/:team_id/users`

##### 响应
```json
{
    "team_users": [
        {
            "id": 6,
            "user_id": 1,
            "team_id": 1,
            "created_at": "2017-07-31T03:12:52.000Z",
            "updated_at": "2017-07-31T03:12:52.000Z"
        },
        {
            "id": 7,
            "user_id": 5,
            "team_id": 1,
            "created_at": "2017-07-31T03:12:52.000Z",
            "updated_at": "2017-07-31T03:12:52.000Z"
        }
    ]
}
```

### ~~小组删除用户~~
`DELTE api/teams/:id/del`

参数名 | 是否必需 | 描述 | 备注
----- | ----- | ---- | ---
user_ids | 是 | 用户 id | 例如: 1,2,3,4

> 建议使用 `DELETE /api/teams/:team_id/users`

##### 响应
```json
{

}
```
