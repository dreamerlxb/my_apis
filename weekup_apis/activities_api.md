# Activity

### 获取 activities

`GET /api/activities`

参数名 | 类型 | 是否必需 | 描述 | 备注
----- | --- | ---- | ----- | -----


### 获取 activity

`GET /api/activities/:id`

参数名 | 是否必需 | 描述
----- | ------ | -----
