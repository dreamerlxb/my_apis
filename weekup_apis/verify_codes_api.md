# VerifyCodes

- 创建一个 verify_code
  `POST  /api/verify_codes`

参数名 | 是否必需 | 描述 | 备注
----- | ----- | ---- | ---
phone | 是 | 手机号 |
action_type | 是 | 类型 | login、regist、verify

- 验证 verify_code
  `POST  /api/verify_codes/verify`

参数名 | 是否必需 | 描述 | 备注
----- | ------ | ----- | ---
phone | 是 | 手机号 |
action_type | 是 | 类型 | login、regist、verify
code | 是 | 验证码 |
