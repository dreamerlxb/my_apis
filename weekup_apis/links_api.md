# Links

-  创建 link
 `POST /api/links`

参数名 | 是否必需 | 描述 | 备注
----- | --------| ----- | ---
title | 是 | title |
url | 是 | link url |
user_id | 是 | user id |
team_ids | false | team ids | 例如： 1,2,3

- 修改 link 信息
  `PUT  /api/links/:id`

参数名 | 是否必需 | 描述 | 备注
----- | --------| ----- | ---
id | true | link id |
title | false | title |
url | false | url |
team_ids | false | team ids | 例如： 1,2,3


-  删除 link
 `DELETE /api/links/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
id   |   是     | link id

-  获取 link 信息
  `GET /api/links/:id`

参数名 | 是否必需 | 描述
----- | ------- | -------
id   |   是     | link id

##### 响应
~~~json
{
}
~~~


- 获取 links 信息
  `GET  /api/links`

参数名 | 是否必需 | 描述
----- | --------| -------
