# Attachments

- 创建 attachment
  `POST /api/attachments`

参数名 | 是否必需 | 描述 | 备注
----- | --------| ---- | ---
name | 是 | 对应的文件 |
user_id | 是 | user id |
team_ids | false | team ids | 例如： 1,2,3

- 删除 attachment
 `DELETE /api/attachments/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
id   |   是     | attachment id

- 获取 attachment 信息
  `GET /api/attachments/:id`

参数名 | 是否必需 | 描述
----- | ------- | -------
id   |   是     | attachment id

##### 响应
~~~json
{
}
~~~

- 获取 attachments 信息
  `GET  /api/attachments`

参数名 | 是否必需 | 描述
----- | --------| -------


- 点赞 attachment
  `POST /api/attachments/:id/like`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | attachment id


- 取消点赞 attachment
  `POST /api/attachments/:id/unlike`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | attachment id





---

#### 分页参数

参数名 | 是否必需 | 描述
----- | --------| -------
page | 是 | 第几页
per_page | 是 | 每页几条记录
