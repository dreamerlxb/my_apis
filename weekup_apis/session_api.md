# sessions

### 创建一个 `session`
`POST  /api/sessions`
> 用户登陆

参数名 | 是否必需 | 描述
----- | --------| -------
phone | true | 手机号
password | true | 密码

##### 响应

```json
{
  "user" : {
    "avatar" : "http:\/\/placehold.it\/60x60?text=L",
    "admin" : false,
    "id" : 9,
    "created_at" : "2017-07-18T04:17:44.000Z",
    "email" : null,
    "updated_at" : "2017-07-18T04:17:44.000Z",
    "name" : null
  },
  "access_token" : {
    "id" : 10,
    "user_id" : 9,
    "updated_at" : "2017-07-18T09:13:00.000Z",
    "token" : "hA1HXexBc8LjBtHGaWultrqekDcrfeQi0Q323rlBUW+hgIjuTj2KcjGfGifFNaQg\/E5\/k7yp1PjdRGXfbVJrVw==",
    "created_at" : "2017-07-18T09:13:00.000Z"
  }
}
```

### 删除 `session`
`DELETE  /api/sessions`
> 退出登陆
