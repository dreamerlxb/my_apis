# Checklists


###  创建 checklist
  `POST /api/checklists`

参数名 | 类型 | 是否必需 | 描述 | 备注
----- | ---- | ------| ---- | ----
team_ids | string | 是 | 该checklist属于那些小组 | 例如: '1,2,3'
next_week | bool | false | 是否为下一周 | true/false, 默认为falee
items |json 字符串 | false | 该checklist所包含的items | 例如:[{"content":"=====","deadline":"2017-08-03 12:00:00"}]
user_id | integer | 是 | user id | 谁创建的
next_week | bool | false | 是否为下一周 | true/false, 默认为false


### 获取一个checklist
`GET /api/checklists/:id`

##### 响应
```json

{
    "id": 1,
    "user_id": 2,
    "activity_id": 41,
    "year": 2017,
    "month": 8,
    "week": 1,
    "created_at": "2017-08-03T02:17:58.000Z",
    "updated_at": "2017-08-03T02:17:58.000Z",
    "items": [
        {
            "id": 1,
            "user_id": 2,
            "content": "test-01test-01test-01test-01test-01test-01test-01test-01test-01",
            "checked": false,
            "deadline": "2017-08-02T10:00:00.000Z",
            "created_at": "2017-08-01T10:00:00.000Z",
            "updated_at": "2017-08-01T10:00:00.000Z"
        }
    ]
}
```

### 获取所有的 `checklists`
`GET    /api/checklists`

### 修改 `checklist` 信息
`PUT    /api/checklists/:id`

参数名 | 类型 | 是否必需 | 描述 | 备注
----- | ---- | ------| ---- | ----
team_ids | string | 是 | 该checklist属于那些小组 | 例如: '1,2,3'

>注意:对于checklist, 只可以修改他属于那些小组,而其他信息,一旦创建不可修改

### 删除一个 `checklist`
`DELETE /api/checklists/:id`
> 注意,一旦删除该checklist,那么该checklist下的所有items也将被删除


### 获取某个 `checklist` 下的所有 `items`
`GET  /api/checklists/:checklist_id/items`

##### 响应
```json
[
    {
        "id": 1,
        "user_id": 2,
        "content": "test-01test-01test-01test-01test-01test-01test-01test-01test-01",
        "checked": false,
        "deadline": "2017-08-02T10:00:00.000Z",
        "created_at": "2017-08-01T10:00:00.000Z",
        "updated_at": "2017-08-01T10:00:00.000Z"
    }
]
```

### 为 `checklist` 添加 `items`
`POST   /api/checklists/:checklist_id/items`

参数名 | 类型 | 是否必需 | 描述 | 备注
----- | ---- | ------| ---- | ----
items |json 字符串 | false | 该checklist所包含的items | 例如:[{"content":"=====","deadline":"2017-08-03 12:00:00"}]

### 修改 `checklist` 的某个 `item`
`PUT   /api/checklists/:checklist_id/items/:id`

参数名 | 类型 | 是否必需 | 描述 | 备注
----- | ---- | ------| ---- | ----
content | string | true | 内容 |
deadline | date | true |  |
checked | bool | false |是否 checked | 默认false

### 删除checklist的某个item
`DELETE /api/checklists/:checklist_id/items/:id`
