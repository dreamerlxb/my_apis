# Photos

-  Create a photo
  `POST /api/photos`

参数名 | 是否必需 | 描述
----- | --------| -------
image | 是 | 图片
user_id | 是 | user id(该用户所创建)
