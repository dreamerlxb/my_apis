# `Ups`

### 获取 `ups` 信息

`GET  /api/ups`

参数名 | 类型 | 是否必需 | 描述 | 备注
----- | ---- | ---- |----|---
year| integer|false||
month| integer|false||
week| integer|false||
team_id | integer | true | 被up的小组的id|

> 所有的up都是以小组为标准的，所以只可以获取某一组的ups

##### 响应
```json
{
    "paginate_meta": {
        "current_page": 1,
        "next_page": null,
        "prev_page": null,
        "total_pages": 1,
        "total_count": 6
    },
    "ups": [
        {
            "year": 2017,
            "month": 8,
            "week": 2,
            "upped_id": 4,
            "up_counts": 6, 
            "upped_user": {
                "id": 4,
                "email": null,
                "name": "test-03",
                "upped": true,
                "phone": "13465425938",
                "avatar": null,
                "created_at": "2017-07-31T01:57:38.000Z",
                "updated_at": "2017-07-31T01:57:38.000Z"
            }
        }
    ]
}
```

### 获取某小组某用户被up的次数

`GET /api/ups/count`

参数名 | 类型 | 是否必需 | 描述 | 备注
----- | ---- | ---- |----|---
year| integer|false||
month| integer|false||
week| integer|false||
team_id | integer | true | 被up的用户所在小组id|
user_id | integer | true | 被up的用户id|


###### 响应
```json
{
    count: 3
}
```

### 获取某一个 `up` 信息

`GET /api/ups/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
id   |   是     | up id

> 目前来看，这个请求并没有什么意义

##### 响应
```json
{
}
```

### 创建 `up`

`POST /api/ups`

参数名 | 参数类型 | 是否必需 | 描述
----- | ------ | ------- | ----
upped_id| integer | 是 | 被up的 user id
team_id | integer |是 | 被up用户所在的 team 的 id
user_id | integer | 是 | 谁 up 的

###### 响应
```json
{
    "id": 1,
    "year": 2017,
    "month": 8,
    "week": 2,
    "upped_id": 4,
    "user_id": 2,
    "team_id" 1,
    "created_at": "2017-07-31T01:57:38.000Z",
    "updated_at": "2017-07-31T01:57:38.000Z"
}
```
