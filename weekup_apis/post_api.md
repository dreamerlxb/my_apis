# posts

- 获取 Posts 信息
`GET  /api/posts`

参数名 | 是否必需 | 描述
----- | --------| -------


- 修改 post 信息
`PUT  /api/posts/:id`

参数名 | 是否必需 | 描述 | 备注
----- | --------| ----- | ---
title | 是 | post title | 
content | 是 | post content |
team_ids | false | 该post属于哪个team | 例如：1,2,3 

-  创建 Post
 `POST /api/posts`

参数名 | 是否必需 | 描述 | 备注
----- | --------| ----- | ---
title | 是 | post title |
content | 是 | post content |
user_id | 是 | user id |
team_ids | false | 该post属于哪个team | 例如：1,2,3 

##### 响应
```json
{
    "id":1,
    "title":"",
    "content":"",
    "user_id":1,
    "activity_id":2,
    "created_at": "",
    "updated_at": ""
}
```


-  删除 Post
 `DELETE /api/posts/:id`

参数名 | 是否必需 | 描述
----- | --------| -------

- 点赞 post
`POST /api/posts/:id/like`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | 用户id


- 取消点赞 post
 ` POST /api/posts/:id/unlike`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | 用户id

- 收藏 post
`POST /api/posts/:id/favorite`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | 用户id


-  取消收藏 post
`POST /api/posts/:id/unfavorite`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | 用户id

## 响应
~~~json
~~~

-  获取用户信息
`GET /api/posts/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
id   |   是     | 用户 id

##### 响应
```json
{
    "id":1,
    "title":"",
    "content":"",
    "user_id":1,
    "activity_id":2,
    "created_at": "",
    "updated_at": "",
    "author": {
        ...
    }
}
```
