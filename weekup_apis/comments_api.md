# Comments

- 创建 comment
  `POST /api/comments`

参数名 | 是否必需 | 描述 | 备注
----- | ----- | ----- | ---
content | 是 | 内容 |
user_id | 是 | user id |
target_id | 是 | 评论的目标的id |
target_type | 是 | 评论的目标 | 例如：post, link等

- 删除 comment
 `DELETE /api/comments/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
id   |   是     | comment id

- 获取 comment 信息
  `GET /api/comments/:id`

参数名 | 是否必需 | 描述
----- | ------- | -------
id   |   是     | comment id

##### 响应
~~~json
{
}
~~~

- 获取 comments 信息
  `GET  /api/comments`

参数名 | 是否必需 | 描述
----- | --------| -------


---

#### 分页参数

参数名 | 是否必需 | 描述
----- | --------| -------
page | 是 | 第几页
per_page | 是 | 每页几条记录
