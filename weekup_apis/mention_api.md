# Mentions

### 创建 mention
  `POST /api/mentions`

参数名 | 是否必需 | 描述 | 备注
----- | --------| ----- | ---
message | 是 | mention message |
mentioned_ids | 是 | 例如：1,2,3 |
user_id | 是 | user id |
team_ids | false | team ids | 例如： 1,2,3


### 修改 mention 信息
  `PUT  /api/mentions/:id`

参数名 | 是否必需 | 描述 |
----- | --------| ---- | ---
message | 是 | mention message |
mentioned_ids | 是 |提及到的 mentioned_ids|
team_ids | false | team ids | 例如： 1,2,3

### 删除 mention
 `DELETE /api/mentions/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
id   |   是     | mention id

### 获取 mention 信息
  `GET /api/mentions/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
id   |   是     | mention id

##### 响应
~~~json
{
}
~~~

### 获取 mentions 信息
  `GET  /api/mentions`

参数名 | 是否必需 | 描述
----- | -------- | -------
