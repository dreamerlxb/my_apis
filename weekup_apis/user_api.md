# users

### 修改 `user` 信息
`PUT  /api/users/:id`
> 目前只支持修改 `name` 和 `email`

参数名 | 类型 | 是否必需 | 描述
----- | ---- | ------| -------
name | string | false | 用户 name
email | string | false | 用户 email

##### 响应
```json
{
     "id": 1,
"email": "test-user-00@mail.com",
"name": "newbie",
"admin": false,
"upped": true,
"avatar": "http://placehold.it/60x60?text=L",
"created_at": "2017-07-03T02:48:32.000Z",
"updated_at": "2017-07-11T06:17:43.000Z"
}
```

### 创建 `user`
`POST /api/users`

参数名 | 类型 | 是否必需 | 描述 | 备注
----- | ---- | --------| ---- | ---
name | string | false | 用户name | 
password | string | 是 | 用户password |
phone |string | 是 | 用户phone |
code |string| 是 | 验证码 |

>  错误响应 `{ "ok": false, "msg": "验证码错误", "error": "验证码错误", "status": 441 }`

##### 响应
```json
{
    "user": {
        "id": 1,
        "email": "test-user-00@mail.com",
        "name": "newbie",
        "admin": false,
        "upped": true,
        "avatar": "http://placehold.it/60x60?text=L",
        "created_at": "2017-07-03T02:48:32.000Z",
        "updated_at": "2017-07-11T06:17:43.000Z"
    },
    "access_token": {
        "id": 1,
        "access_token": "xxxxxxxxxxxxxxxxxxx",
        ....
    }
}

```

### 获取 `users`
`GET api/users/`

参数名 | 类型 | 是否必需 | 描述 | 备注
----- | --- | ------ | ----- | ----
name   | string |   false   | 用户 name | 筛选条件
email  |string| false   | 用户 email | 筛选条件
phone  |string| false   | 用户 phone | 筛选条件

##### 响应
```json
[{
    "id": 1,
"email": "test-user-00@mail.com",
"name": "newbie",
"admin": false,
"upped": true,
"avatar": "http://placehold.it/60x60?text=L",
"created_at": "2017-07-03T02:48:32.000Z",
"updated_at": "2017-07-11T06:17:43.000Z"
}]
```

### 获取 `user` 信息
`GET /api/users/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
id   |   是     | 用户 id

#### 响应
```json
{
"id": 1,
"email": "test-user-00@mail.com",
"name": "newbie",
"admin": false,
"upped": true,
"avatar": "http://placehold.it/60x60?text=L",
"created_at": "2017-07-03T02:48:32.000Z",
"updated_at": "2017-07-11T06:17:43.000Z"
}
```

### 上传 `user` 头像
`POST users/:id/avatar`

参数名 | 类型 | 是否必需 | 描述
----- | ---- | -----| -------
avatar | file | 是 | 用户avatar


### 用户的 `posts`
`GET /api/users/:id/posts`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | user id

### 用户的 `links`
`GET /api/users/:id/links`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | user id


### 用户的 `attachments`
`GET /api/users/:id/attachments`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | user id


### 用户的 `mentions`
`GET /api/users/:id/mentions`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | user id


### 用户的 `summaries`
`GET /api/users/:id/summaries`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | user id


### 用户的 `activities`
`GET /api/users/:id/activities`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | user id

##### 响应
~~~json
{
    "paginate_meta": {
        "current_page": 1,
        "next_page": null,
        "prev_page": null,
        "total_pages": 1,
        "total_count": 15
    },
    "activities": [
        {
            "id": 41,
            "user_id": 2,
            "activity_type": "checklist",
            "created_at": "2017-08-03T02:17:58.000Z",
            "updated_at": "2017-08-03T02:17:58.000Z",
        },
        {
            "id": 42,
            "user_id": 2,
            "activity_type": "link",
            "created_at": "2017-08-03T02:17:58.000Z",
            "updated_at": "2017-08-03T02:17:58.000Z",
        }
    ]
}
~~~


### 用户的 `checklists`
`GET /api/users/:id/checklists`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | user id

##### 响应
~~~json
{
    "paginate_meta": {
        "current_page": 1,
        "next_page": null,
        "prev_page": null,
        "total_pages": 1,
        "total_count": 1
    },
    "checklists": [
        {
            "id": 1,
            "user_id": 2,
            "activity_id": 41,
            "year": 2017,
            "month": 8,
            "week": 1,
            "created_at": "2017-08-03T02:17:58.000Z",
            "updated_at": "2017-08-03T02:17:58.000Z",
            "items": [
                {
                    "id": 1,
                    "user_id": 2,
                    "content": "test-01test-01test-01test-01test-01test-01test-01test-01test-01",
                    "checked": false,
                    "deadline": "2017-08-02T10:00:00.000Z",
                    "created_at": "2017-08-01T10:00:00.000Z",
                    "updated_at": "2017-08-01T10:00:00.000Z"
                }
            ]
        }
    ]
}
~~~


### 用户所在的小组(包括用户自己创建的小组)
`GET /api/users/:id/teams`

参数名 | 是否必需 | 描述
----- | ----- | ----
id | 是 | user id

##### 响应
```json
{
    "paginate_mate": {
        .....
    },
    "teams":[{
        "id":1,
        "name":"",
        "owner_id":1,
        "logo": "",
        "description":"描述",
        "created_at": "",
        "updated_at": ""
    }, {
        "id":2,
        "name":"",
        "owner_id":1,
        "logo": "",
        "description":"描述",
        "created_at": "",
        "updated_at": ""
    }]  
}
```

### 重置密码
`POST /api/users/:id/reset_pwd`

参数名 | 是否必需 | 描述
----- | --------| -------
id | 是 | 用户id
old_password  |  是  | 旧密码
new_password | 是 | 新密码

#### 响应
~~~json
{
  "ok": true,
  "error": "错误信息"
}
~~~
