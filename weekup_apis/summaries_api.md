# Summaries

-  创建 summary
 `POST /api/summaries`

参数名 | 是否必需 | 描述 | 备注
----- | --------| ----- | ---
content | false | summary content | 
stars | false | 例如：4.5 |
user_id | true | user id |
team_ids | false | team ids | 例如： 1,2,3

- 修改 summary 信息
  `PUT  /api/summaries/:id`

参数名 | 是否必需 | 描述 | 备注
----- | --------| ----- | ---
id | true | summary id |
content | false | summary content |
stars | false | 例如：4.5 |
team_ids | false | team ids | 例如： 1,2,3


-  删除 summary
  `DELETE /api/summaries/:id`

参数名 | 是否必需 | 描述
----- | --------| -------
id   |   是     | summary id

-  获取 summary 信息
  `GET /api/summaries/:id`

参数名 | 是否必需 | 描述
----- | ------- | -------
id   |   true  | summary id

## 响应
```json
{
    "id": 1,
    "user_id": 1,
    "activity_id": 53,
    "stars": 4.5,
    "content": "my first summary",
    "week": 2,
    "month": 8,
    "year": 2017,
    "created_at": "2017-08-09T08:08:13.000Z",
    "updated_at": "2017-08-09T08:08:13.000Z"
}
```


- 获取 summaries 信息
  `GET  /api/summaries`

参数名 | 是否必需 | 描述 | 备注
----- | ------- | ---- | ---

```json

{
    "paginate_meta": {
        "current_page": 1,
        "next_page": null,
        "prev_page": null,
        "total_pages": 1,
        "total_count": 1
    },
    "summaries": [
        {
            "id": 1,
            "user_id": 1,
            "activity_id": 53,
            "stars": 4.5,
            "content": "my first summary",
            "week": 2,
            "month": 8,
            "year": 2017,
            "created_at": "2017-08-09T08:08:13.000Z",
            "updated_at": "2017-08-09T08:08:13.000Z"
        }
    ]
}
```
