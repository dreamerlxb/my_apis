# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.18)
# Database: weekup_dev
# Generation Time: 2017-12-01 07:05:42 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table access_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `access_tokens`;

CREATE TABLE `access_tokens` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_access_token` (`token`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `access_tokens` WRITE;
/*!40000 ALTER TABLE `access_tokens` DISABLE KEYS */;

INSERT INTO `access_tokens` (`id`, `user_id`, `token`, `created_at`, `updated_at`)
VALUES
	(1,8,'Iw3enpiO0dGOEEcc1G6tmlVZR+DB/T+2d0dTmuSrTGkcrPgJjIkGUZ9De5f8MB5u28erPGRZj8kix5Y0xE8t3w==','2017-07-11 07:06:23','2017-07-11 07:06:23'),
	(2,1,'1MAhevI0X8siRl9IGyEVmQ7CwpkdFJLZA7tyS/zKTvquvIxtSFH7qNj0/lmvHlbumAq1049ZGVV5L0lS7P63EA==','2017-07-11 07:57:53','2017-07-11 07:57:53'),
	(3,1,'4qT8cl0RJ7U/SQ7xNEtIBlbvdHIqrRZsT0xNv+G4HZvoJPUoK9bblyHv4FNDvYv5ky4XJo7nK7ZSYUBCTwvarw==','2017-07-18 03:06:36','2017-07-18 03:06:36'),
	(4,9,'pcrQ3WHUu5IczlAqq8prUn8Uk0iOm0RMtviJf7gG0ngNLslcFT9LplYx21ZHFKvknau71hsf7e70HvjZJpf3hw==','2017-07-18 04:17:44','2017-07-18 04:17:44'),
	(5,9,'c2hD6afvR2QiiD7f8cV+jAEZWWP2F3g9yKdGi0CzjLfJZZlAfPKid+4lFXr4gpw/PN8cqcYP5bCFpZ9A5tUkjw==','2017-07-18 04:30:35','2017-07-18 04:30:35'),
	(6,9,'IUKBgOjM7yBRdv55hY6tVSu69lftVNafJTprtVtW19d5mOzg5fO6ntOjkJU9aQZnuRdfxL9nGI+X56k3w7fvWw==','2017-07-18 04:31:17','2017-07-18 04:31:17'),
	(7,9,'/LXKkiHXWnSwQopOZQ3UK6I2KyfHgQd5niZI/MHYxLRrHokPPLs9LUbPujny3Ljqw5/TMO+TlsUZTPHNiL0maw==','2017-07-18 04:31:55','2017-07-18 04:31:55'),
	(8,1,'/H+/gxcmf97lJuZCmCRVa8xcuJt4goWd2ymBQG0oo5ZHU5MLkYttxJVR5b6omXo2CFdIa1xY6PPSgoCfuM5DLQ==','2017-07-18 04:32:32','2017-07-18 04:32:32'),
	(9,9,'wQpx8QmgH6JRHxkdidE0yOoQD1C+ckzO9qFeiTDm4h0gUULo7ZTu8Y4aD1U/624Ob6DJkIclvx8xkfQuLMamVg==','2017-07-18 04:34:59','2017-07-18 04:34:59'),
	(10,9,'hA1HXexBc8LjBtHGaWultrqekDcrfeQi0Q323rlBUW+hgIjuTj2KcjGfGifFNaQg/E5/k7yp1PjdRGXfbVJrVw==','2017-07-18 09:13:00','2017-07-18 09:13:00'),
	(11,9,'0ff143YmzXwZ3b1boqvlMeCDHDH9apWrRAJF5ClKVzla1hZPZOMKhQgb608QFrC6E2awL6wCFVFzexYfAQZ5uA==','2017-07-24 06:51:06','2017-07-24 06:51:06'),
	(12,9,'iKgHL/tcmaYHoLJ3NOoUq+gomksbi/M+7XiwH+/ANCcOJvHAa4uVxG/ohEM7X2aTL0BkpVQW+XJXx0HFqBPrCg==','2017-07-25 02:07:43','2017-07-25 02:07:43'),
	(13,9,'Nm1p8Zid4YJ1rcNjG/emwjV+ciWND+Xbsgdt+jtySTpT6jKUqTbHxU+7hwyfdVeEDJW81QDZs/RxNBBHaT1BzA==','2017-07-25 02:08:54','2017-07-25 02:08:54'),
	(14,9,'KRip4ntVeMkL/gxjxhEbyT4Xa7IY2Y64kI40sKQeMRaUn717aIeGTkERqoRDE+NoEQ1HdNNYbeYvTtXHXgyY3g==','2017-07-25 02:17:17','2017-07-25 02:17:17'),
	(15,9,'c1eYRYTS4KKKVotKiTFbFA0iGUK1+jzsIV/+wThhjzzWu8r5t15JcxahWdNgg55/PjWo20bM2r0f6WjQNFeFGg==','2017-07-25 02:19:56','2017-07-25 02:19:56'),
	(16,9,'JLhVIIuvWqmuEr7lJqc2ttvcako24vRhltaa1l79EGm6mVNM1Vb4YAv/Rj+pNe3j0m8Gas66l0/Tg3Lf+PoVTQ==','2017-07-25 02:22:42','2017-07-25 02:22:42'),
	(17,9,'WqbJBGYpzgwYDlwxY0lxkgbveTWH4vPGpTIYfcG88q7jHHUJL7X4jeviCtBspeohedZ/RYHm+npAo2iIH0HfrA==','2017-07-25 08:05:34','2017-07-25 08:05:34'),
	(18,10,'sGAKxRBdI2O0SizPOClfvgYTXyRmOATnTsNVWpDxGXr+yHP1wuijkCDXH96CRTvr9aBV6p8Cc5LVEQ+iTgZKBA==','2017-12-01 06:26:14','2017-12-01 06:26:14'),
	(19,10,'9//BwmySgIILmHa3zS8o5tchzw85tGafiee/fc13i0Y0+rY1gu8e73Qml80W6u+OG/MLv9AyMe0n3YXrq2Q+2g==','2017-12-01 06:38:50','2017-12-01 06:38:50');

/*!40000 ALTER TABLE `access_tokens` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table actions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `actions`;

CREATE TABLE `actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_type` varchar(255) NOT NULL,
  `action_option` varchar(255) DEFAULT NULL,
  `target_type` varchar(255) DEFAULT NULL,
  `target_id` int(11) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_actions_on_user_type_and_user_id_and_action_type` (`user_type`,`user_id`,`action_type`),
  KEY `index_actions_on_target_type_and_target_id_and_action_type` (`target_type`,`target_id`,`action_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `actions` WRITE;
/*!40000 ALTER TABLE `actions` DISABLE KEYS */;

INSERT INTO `actions` (`id`, `action_type`, `action_option`, `target_type`, `target_id`, `user_type`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1,'like',NULL,'Post',1,'User',1,'2017-07-06 07:19:07','2017-07-06 07:19:07'),
	(2,'like',NULL,'Post',4,'User',3,'2017-07-06 07:51:38','2017-07-06 07:51:38'),
	(3,'star',NULL,'Post',4,'User',3,'2017-07-06 07:52:23','2017-07-06 07:52:23');

/*!40000 ALTER TABLE `actions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table activities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `activities`;

CREATE TABLE `activities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activity_type` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `target_type` varchar(255) DEFAULT NULL,
  `target_value` varchar(255) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `mweek` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_activity_on_target` (`target_value`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;

INSERT INTO `activities` (`id`, `activity_type`, `created_at`, `updated_at`, `user_id`, `target_type`, `target_value`, `year`, `month`, `mweek`)
VALUES
	(1,0,'2017-07-03 07:30:46','2017-07-03 07:30:46',1,NULL,NULL,NULL,NULL,NULL),
	(4,0,'2017-07-03 07:30:46','2017-07-03 07:30:46',1,NULL,NULL,NULL,NULL,NULL),
	(6,0,'2017-07-03 07:30:46','2017-07-17 08:29:16',1,NULL,NULL,NULL,NULL,NULL),
	(7,0,'2017-07-03 07:30:46','2017-07-17 08:34:58',1,NULL,NULL,NULL,NULL,NULL),
	(8,0,'2017-07-03 07:30:46','2017-07-03 07:30:46',1,NULL,NULL,NULL,NULL,NULL),
	(9,0,'2017-07-03 07:30:46','2017-07-03 07:30:46',1,NULL,NULL,NULL,NULL,NULL),
	(10,0,'2017-07-03 07:30:46','2017-07-03 07:30:46',1,NULL,NULL,NULL,NULL,NULL),
	(11,0,'2017-07-03 07:30:46','2017-07-03 07:30:46',1,NULL,NULL,NULL,NULL,NULL),
	(12,1,'2017-07-03 08:30:46','2017-07-03 08:30:46',1,NULL,NULL,NULL,NULL,NULL),
	(13,1,'2017-07-03 08:30:46','2017-07-03 08:30:46',1,NULL,NULL,NULL,NULL,NULL),
	(14,1,'2017-07-03 08:30:46','2017-07-03 08:30:46',1,NULL,NULL,NULL,NULL,NULL),
	(15,1,'2017-07-03 08:30:46','2017-07-03 08:30:46',1,NULL,NULL,NULL,NULL,NULL),
	(16,1,'2017-07-03 08:30:46','2017-07-03 08:30:46',1,NULL,NULL,NULL,NULL,NULL),
	(17,1,'2017-07-03 08:30:46','2017-07-03 08:30:46',1,NULL,NULL,NULL,NULL,NULL),
	(18,1,'2017-07-03 08:30:46','2017-07-03 08:30:46',1,NULL,NULL,NULL,NULL,NULL),
	(19,0,'2017-07-04 08:30:46','2017-07-04 08:30:46',2,NULL,NULL,NULL,NULL,NULL),
	(20,0,'2017-07-04 08:30:46','2017-07-04 08:30:46',2,NULL,NULL,NULL,NULL,NULL),
	(21,0,'2017-07-04 08:30:46','2017-07-04 08:30:46',2,NULL,NULL,NULL,NULL,NULL),
	(22,0,'2017-07-04 08:30:46','2017-07-04 08:30:46',2,NULL,NULL,NULL,NULL,NULL),
	(23,0,'2017-07-04 08:30:46','2017-07-04 08:30:46',2,NULL,NULL,NULL,NULL,NULL),
	(24,1,'2017-07-04 08:30:46','2017-07-04 08:30:46',2,NULL,NULL,NULL,NULL,NULL),
	(25,1,'2017-07-04 08:30:46','2017-07-04 08:30:46',2,NULL,NULL,NULL,NULL,NULL),
	(26,1,'2017-07-04 08:30:46','2017-07-04 08:30:46',2,NULL,NULL,NULL,NULL,NULL),
	(27,1,'2017-07-04 08:30:46','2017-07-04 08:30:46',2,NULL,NULL,NULL,NULL,NULL),
	(28,1,'2017-07-04 08:30:46','2017-07-04 08:30:46',2,NULL,NULL,NULL,NULL,NULL),
	(31,0,'2017-07-04 06:49:54','2017-07-04 06:49:54',2,NULL,NULL,NULL,NULL,NULL),
	(32,0,'2017-07-04 06:51:00','2017-07-04 06:51:00',2,NULL,NULL,NULL,NULL,NULL),
	(33,0,'2017-07-04 07:33:27','2017-07-04 07:33:27',2,NULL,NULL,NULL,NULL,NULL),
	(34,1,'2017-07-04 07:38:36','2017-07-04 07:38:36',2,NULL,'',NULL,NULL,NULL),
	(36,3,'2017-07-04 08:29:10','2017-07-04 08:29:10',2,NULL,'',NULL,NULL,NULL),
	(37,3,'2017-07-04 08:29:54','2017-07-04 08:29:54',2,NULL,NULL,NULL,NULL,NULL),
	(38,0,'2017-07-04 09:26:16','2017-07-04 09:26:16',2,NULL,NULL,NULL,NULL,NULL),
	(43,2,'2017-07-06 08:21:47','2017-07-06 08:21:47',2,NULL,NULL,NULL,NULL,NULL),
	(44,2,'2017-07-06 08:39:38','2017-07-06 08:39:38',4,NULL,NULL,NULL,NULL,NULL),
	(48,3,'2017-07-14 04:16:01','2017-07-14 04:16:01',1,NULL,'2017-7-2',NULL,NULL,NULL),
	(49,3,'2017-07-14 06:22:21','2017-07-17 09:23:27',1,NULL,'2017-7-3',NULL,NULL,NULL);

/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ar_internal_metadata
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ar_internal_metadata`;

CREATE TABLE `ar_internal_metadata` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ar_internal_metadata` WRITE;
/*!40000 ALTER TABLE `ar_internal_metadata` DISABLE KEYS */;

INSERT INTO `ar_internal_metadata` (`key`, `value`, `created_at`, `updated_at`)
VALUES
	('environment','development','2017-07-03 02:39:20','2017-07-03 02:39:20');

/*!40000 ALTER TABLE `ar_internal_metadata` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table attachments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attachments`;

CREATE TABLE `attachments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `attachment_type` varchar(255) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `likes_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `attachments` WRITE;
/*!40000 ALTER TABLE `attachments` DISABLE KEYS */;

INSERT INTO `attachments` (`id`, `name`, `attachment_type`, `size`, `user_id`, `activity_id`, `created_at`, `updated_at`, `likes_count`)
VALUES
	(2,'upload.html',NULL,NULL,2,43,'2017-07-06 08:21:47','2017-07-06 08:21:47',0),
	(3,'AOC.docx',NULL,1137949,4,44,'2017-07-06 08:39:38','2017-07-06 08:39:38',0);

/*!40000 ALTER TABLE `attachments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table checklist_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `checklist_items`;

CREATE TABLE `checklist_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` text,
  `checked` tinyint(1) DEFAULT '0',
  `deadline` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `mweek` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `checklist_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `checklist_items` WRITE;
/*!40000 ALTER TABLE `checklist_items` DISABLE KEYS */;

INSERT INTO `checklist_items` (`id`, `content`, `checked`, `deadline`, `created_at`, `updated_at`, `mweek`, `year`, `user_id`, `activity_id`, `month`, `checklist_id`)
VALUES
	(13,'okokokkok',0,'2017-07-14 16:00:00','2017-07-14 04:16:01','2017-07-14 04:16:01',2,2017,1,48,7,NULL),
	(14,'okoko========',0,'2017-07-14 16:00:00','2017-07-14 04:27:11','2017-07-14 04:27:11',2,2017,1,48,7,NULL),
	(15,'okoko========',0,'2017-07-14 16:00:00','2017-07-14 04:29:20','2017-07-14 04:29:20',2,2017,1,48,7,NULL),
	(16,'修改checklist_items的表结构',0,'2017-07-14 15:30:00','2017-07-14 06:21:10','2017-07-14 06:21:10',2,2017,1,48,7,NULL),
	(17,'调整论坛页面结构',0,'2017-07-14 15:30:00','2017-07-14 06:22:21','2017-07-14 06:22:21',3,2017,1,49,7,NULL),
	(18,'sjbdvsbdvdbvjhdf',0,'2017-07-17 18:00:00','2017-07-17 08:50:42','2017-07-17 08:50:42',3,2017,1,49,7,NULL),
	(19,'abcdefghi',0,'2017-07-17 19:00:00','2017-07-17 09:23:27','2017-07-17 09:23:27',3,2017,1,49,7,NULL);

/*!40000 ALTER TABLE `checklist_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table checklists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `checklists`;

CREATE TABLE `checklists` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `mweek` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `activity_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_checklists_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `content` text,
  `target_type` varchar(255) DEFAULT NULL,
  `target_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table links
# ------------------------------------------------------------

DROP TABLE IF EXISTS `links`;

CREATE TABLE `links` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `links` WRITE;
/*!40000 ALTER TABLE `links` DISABLE KEYS */;

INSERT INTO `links` (`id`, `url`, `user_id`, `created_at`, `updated_at`, `activity_id`, `title`, `img`, `content`)
VALUES
	(1,'https://baidu.com',1,'2017-07-03 09:17:12','2017-07-03 09:17:12',12,NULL,NULL,NULL),
	(2,'https://baidu.com',1,'2017-07-03 09:17:12','2017-07-03 09:17:12',13,NULL,NULL,NULL),
	(3,'https://baidu.com',1,'2017-07-03 09:17:12','2017-07-03 09:17:12',14,NULL,NULL,NULL),
	(4,'https://baidu.com',1,'2017-07-03 09:17:12','2017-07-03 09:17:12',15,NULL,NULL,NULL),
	(5,'https://baidu.com',1,'2017-07-03 09:17:12','2017-07-03 09:17:12',16,NULL,NULL,NULL),
	(6,'https://baidu.com',1,'2017-07-03 09:17:12','2017-07-03 09:17:12',17,NULL,NULL,NULL),
	(7,'https://baidu.com',1,'2017-07-03 09:17:12','2017-07-03 09:17:12',18,NULL,NULL,NULL),
	(8,'https://baidu.com',2,'2017-07-04 09:17:12','2017-07-04 09:17:12',24,NULL,NULL,NULL),
	(9,'https://baidu.com',2,'2017-07-04 09:17:12','2017-07-04 09:17:12',26,NULL,NULL,NULL),
	(10,'https://baidu.com',2,'2017-07-04 09:17:12','2017-07-04 09:17:12',27,NULL,NULL,NULL),
	(11,'https://baidu.com',2,'2017-07-04 09:17:12','2017-07-04 09:17:12',28,NULL,NULL,NULL),
	(12,'https://baidu.com',2,'2017-07-04 09:17:12','2017-07-04 09:17:12',25,NULL,NULL,NULL),
	(13,'https://taobao.com',2,'2017-07-04 07:38:36','2017-07-04 07:38:36',34,'我的github',NULL,NULL);

/*!40000 ALTER TABLE `links` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mentions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mentions`;

CREATE TABLE `mentions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `mentioned_ids` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mentions` WRITE;
/*!40000 ALTER TABLE `mentions` DISABLE KEYS */;

INSERT INTO `mentions` (`id`, `message`, `user_id`, `activity_id`, `mentioned_ids`, `created_at`, `updated_at`)
VALUES
	(1,'我@了你们，请回复',2,36,'1,3,4','2017-07-04 08:29:10','2017-07-04 08:29:10'),
	(2,'再次@了你们，请回复',2,37,'1','2017-07-04 08:29:54','2017-07-04 08:29:54'),
	(3,'@you',2,48,'3','2017-07-04 08:23:12','2017-08-12 09:11:12'),
	(4,'@sdjnvjsdnv',2,49,'4','2017-08-09 12:12:12','2017-09-09 00:00:00');

/*!40000 ALTER TABLE `mentions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table photos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `photos`;

CREATE TABLE `photos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `image_type` varchar(255) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `likes_count` int(11) DEFAULT '0',
  `stars_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;

INSERT INTO `posts` (`id`, `title`, `content`, `created_at`, `updated_at`, `activity_id`, `user_id`, `likes_count`, `stars_count`)
VALUES
	(1,'aasasdsfdhuefbenfbdfuhbdn','svbsnvjivhvb','2017-07-03 07:32:42','2017-07-06 07:19:07',1,1,1,0),
	(4,'Ruby on Rails !!','Ruby on Rails  真的好，真的棒','2017-07-03 07:32:42','2017-07-06 07:52:24',4,1,1,1),
	(6,'test-06 A  post','svbsnvjivhvb-2','2017-07-03 07:32:42','2017-07-17 08:26:43',6,1,0,0),
	(7,'test-07 An Post','svbsnvjivhvb-2','2017-07-03 07:32:42','2017-07-17 08:34:58',7,1,0,0),
	(8,'aasasdsfdhuefbenfbdfuhbdn-2','svbsnvjivhvb-2','2017-07-03 07:32:42','2017-07-03 07:32:42',8,1,0,0),
	(9,'aasasdsfdhuefbenfbdfuhbdn-2','svbsnvjivhvb-2','2017-07-03 07:32:42','2017-07-03 07:32:42',9,1,0,0),
	(10,'aasasdsfdhuefbenfbdfuhbdn-2','svbsnvjivhvb-2','2017-07-03 07:32:42','2017-07-03 07:32:42',10,1,0,0),
	(11,'aasasdsfdhuefbenfbdfuhbdn-2','svbsnvjivhvb-2','2017-07-03 07:32:42','2017-07-03 07:32:42',11,1,0,0),
	(12,'aasasdsfdhuefbenfbdfuhbdn-2','svbsnvjivhvb-2','2017-07-04 07:32:42','2017-07-04 07:32:42',19,2,0,0),
	(13,'aasasdsfdhuefbenfbdfuhbdn-2','svbsnvjivhvb-2','2017-07-04 07:32:42','2017-07-04 07:32:42',20,2,0,0),
	(14,'aasasdsfdhuefbenfbdfuhbdn-2','svbsnvjivhvb-2','2017-07-04 07:32:42','2017-07-04 07:32:42',21,2,0,0),
	(15,'aasasdsfdhuefbenfbdfuhbdn-2','svbsnvjivhvb-2','2017-07-04 07:32:42','2017-07-04 07:32:42',22,2,0,0),
	(16,'aasasdsfdhuefbenfbdfuhbdn-2','svbsnvjivhvb-2','2017-07-04 07:32:42','2017-07-04 07:32:42',23,2,0,0),
	(18,'今天是个好日子','因为今天下雨了','2017-07-04 06:49:54','2017-07-04 06:49:54',31,2,0,0),
	(19,'今天是个好日子2','因为今天下雨了2','2017-07-04 06:51:00','2017-07-04 06:51:00',32,2,0,0),
	(20,'今天是个好日子3','因为今天下雨了3','2017-07-04 07:33:27','2017-07-04 07:33:27',33,2,0,0),
	(21,'今天是个好日子4','因为今天下雨了4','2017-07-04 09:26:16','2017-07-04 09:26:16',38,2,0,0);

/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table schema_migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `schema_migrations`;

CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;

INSERT INTO `schema_migrations` (`version`)
VALUES
	('20170629085331'),
	('20170629091846'),
	('20170629094438'),
	('20170703061644'),
	('20170703063728'),
	('20170703064002'),
	('20170703065524'),
	('20170703065741'),
	('20170703071957'),
	('20170703072133'),
	('20170703072313'),
	('20170703072630'),
	('20170703073459'),
	('20170703073717'),
	('20170704050402'),
	('20170704060341'),
	('20170704060720'),
	('20170704072826'),
	('20170704080808'),
	('20170705063432'),
	('20170705064623'),
	('20170705080722'),
	('20170705083221'),
	('20170705091112'),
	('20170706030417'),
	('20170706033155'),
	('20170706070600'),
	('20170706071352'),
	('20170711015512'),
	('20170711020451'),
	('20170711022832'),
	('20170711062929'),
	('20170711083833'),
	('20170711090257'),
	('20170712074634'),
	('20170713025652'),
	('20170714031523'),
	('20170714061155'),
	('20170717081908'),
	('20170719015513'),
	('20170731080457'),
	('20170801010942'),
	('20170802100638'),
	('20170802101133'),
	('20170803062140'),
	('20170810032957'),
	('20170815011834'),
	('20170815024941'),
	('20170815030759'),
	('20170828085239');

/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table summaries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `summaries`;

CREATE TABLE `summaries` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `content` text,
  `stars` float DEFAULT NULL,
  `mweek` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `month` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table team_activities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_activities`;

CREATE TABLE `team_activities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `team_id` bigint(20) DEFAULT NULL,
  `activity_id` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_team_activities_on_team_id` (`team_id`),
  KEY `index_team_activities_on_activity_id` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table team_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_users`;

CREATE TABLE `team_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `team_id` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `admin` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_team_users_on_user_id` (`user_id`),
  KEY `index_team_users_on_team_id` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `team_users` WRITE;
/*!40000 ALTER TABLE `team_users` DISABLE KEYS */;

INSERT INTO `team_users` (`id`, `user_id`, `team_id`, `created_at`, `updated_at`, `admin`)
VALUES
	(1,1,1,'2017-07-11 10:00:00','2017-07-11 10:00:00',0),
	(2,1,2,'2017-07-11 10:00:00','2017-07-11 10:00:00',0),
	(3,2,2,'2017-07-11 10:00:00','2017-07-11 10:00:00',0),
	(4,3,2,'2017-07-11 10:00:00','2017-07-11 10:00:00',0),
	(5,9,1,'2017-07-19 01:20:37','2017-07-19 01:20:37',0);

/*!40000 ALTER TABLE `team_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table teams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teams`;

CREATE TABLE `teams` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;

INSERT INTO `teams` (`id`, `name`, `owner_id`, `logo`, `description`, `created_at`, `updated_at`)
VALUES
	(1,'A小组',1,NULL,'A小组是Android小组','2017-07-04 12:12:12','2017-07-04 12:12:12'),
	(2,'B小组',1,NULL,'B小组是iOS小组','2017-07-04 13:12:12','2017-07-04 13:12:12'),
	(3,'C小组',1,NULL,'C小组是Node小组','2017-07-03 13:12:12','2017-07-03 13:12:12');

/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ups`;

CREATE TABLE `ups` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `upped_id` int(11) DEFAULT NULL,
  `mweek` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `month` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ups` WRITE;
/*!40000 ALTER TABLE `ups` DISABLE KEYS */;

INSERT INTO `ups` (`id`, `user_id`, `upped_id`, `mweek`, `year`, `team_id`, `created_at`, `updated_at`, `month`)
VALUES
	(1,1,2,28,2017,2,'2017-07-12 01:31:54','2017-07-12 01:31:54',NULL),
	(2,1,3,28,2017,2,'2017-07-12 01:58:45','2017-07-12 01:58:45',NULL);

/*!40000 ALTER TABLE `ups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `activated` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `authentication_token` varchar(255) DEFAULT NULL,
  `password_digest` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `upped` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `name`, `activated`, `created_at`, `updated_at`, `authentication_token`, `password_digest`, `phone`, `avatar`, `upped`)
VALUES
	(1,'test-user-00@mail.com','newbie','2017-07-03 02:48:32','2017-07-03 02:48:32','2017-07-11 06:17:43','Cc7VRAMEhdJaSZ79WhyUQcOMLhdJELm7RgWlfe5w5gp+9GltMr2KcxYnooQcxPk4ZGYJRiuATU8K9N/MPI+5Ag==','$2a$10$MEGr2f9xx7MUFT4L1FNEfua0GyS5dYBG3geEUXa4ZWaqYmLQvQCDW','13465425935',NULL,1),
	(2,'test-user-01@mail.com','test-user-01','2017-07-03 02:48:32','2017-07-03 02:48:32','2017-07-06 05:50:13','Xtzu6igR4zyewP3WLsn7HS66P+UgQbf/e8AlXIXT3LGWcf5UqXy5iGFCrQL/1M1T+X1otoWcSszEfVdGWsLPSQ==','$2a$10$VcroXMacyWb0ksZZuVZfx.xOw09D0330RXj8CJJlh/EKfxUGKbyYG','13465425936','',1),
	(3,'test-user-02@mail.com','test-user-02','2017-07-03 04:48:32','2017-07-03 04:48:32','2017-07-03 04:48:32',NULL,NULL,'13465425945',NULL,1),
	(4,'test-user-03@mail.com','test-user-03','2017-07-03 06:48:32','2017-07-03 06:48:32','2017-07-03 06:48:32',NULL,NULL,'13465425937',NULL,1),
	(5,'test-user-04@mail.com','test-user-04','2017-07-03 08:48:32','2017-07-03 08:48:32','2017-07-03 08:48:32',NULL,NULL,'13465425938',NULL,1),
	(6,'test-user-05@mail.com','test-user-05','2017-07-03 10:48:32','2017-07-03 10:48:32','2017-07-17 07:21:32',NULL,'$2a$10$bHEVjLREhLXH40.cyCq3y.Z1zuGk.8aexPJLjWgNtU.7f6QSsqD5e','13465425939',NULL,1),
	(7,NULL,'lxb',NULL,'2017-07-06 06:25:27','2017-07-06 06:25:29','Yn4mAmpyAYDU5B0kQY4aS6LaTzXSouJFIGt1CBz7DsH2vXr4KEba7T+qpSMGlOZdoBB4SGj0eY2Hu4O32D5h1w==','$2a$10$mqmTS5g9.5.AlrfF/J2b1O/4PYE06phI64hsKx40sS4bYk.coNRau','13465425940',NULL,1),
	(8,NULL,'lvvvvv',NULL,'2017-07-11 07:06:23','2017-07-11 07:06:23',NULL,'$2a$10$XVSpGLYY0/qHvDsLc.9CH.qt5NWzEm6UUHcy/hLGBvTy0YquP/Bu2','13465425941',NULL,1),
	(9,NULL,NULL,NULL,'2017-07-18 04:17:44','2017-07-18 04:17:44',NULL,'$2a$10$Hq.q2OdDnU29RdQ12CQkP.e0NzyTDhOjFMyHKnnRRvnmnudbcVrdW','17865169805',NULL,1),
	(10,NULL,'testtest',NULL,'2017-12-01 06:26:14','2017-12-01 06:26:14',NULL,'$2a$10$SLiKOWYhaU30Tf4/.PYofuMKfOSLeXXVbUfi4eR9gVokfCsmcMnQC','13465425933',NULL,1);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table v_ranks
# ------------------------------------------------------------

DROP VIEW IF EXISTS `v_ranks`;

CREATE TABLE `v_ranks` (
   `year` INT(11) NULL DEFAULT NULL,
   `month` INT(11) NULL DEFAULT NULL,
   `mweek` INT(11) NULL DEFAULT NULL,
   `upped_id` INT(11) NULL DEFAULT NULL,
   `team_id` INT(11) NULL DEFAULT NULL,
   `up_counts` BIGINT(21) NOT NULL DEFAULT '0'
) ENGINE=MyISAM;



# Dump of table verify_codes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `verify_codes`;

CREATE TABLE `verify_codes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `verify_type` int(11) DEFAULT NULL,
  `deadline` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `action` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `verify_codes` WRITE;
/*!40000 ALTER TABLE `verify_codes` DISABLE KEYS */;

INSERT INTO `verify_codes` (`id`, `phone`, `email`, `code`, `verify_type`, `deadline`, `created_at`, `updated_at`, `action`)
VALUES
	(7,'13465425935',NULL,'8293',0,'2017-07-19 08:15:46','2017-07-11 08:14:46','2017-07-11 08:14:46',0);

/*!40000 ALTER TABLE `verify_codes` ENABLE KEYS */;
UNLOCK TABLES;




# Replace placeholder table for v_ranks with correct view syntax
# ------------------------------------------------------------

DROP TABLE `v_ranks`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_ranks`
AS SELECT
   `ups`.`year` AS `year`,
   `ups`.`month` AS `month`,
   `ups`.`mweek` AS `mweek`,
   `ups`.`upped_id` AS `upped_id`,
   `ups`.`team_id` AS `team_id`,count(`ups`.`upped_id`) AS `up_counts`
FROM `ups` group by `ups`.`year`,`ups`.`month`,`ups`.`mweek`,`ups`.`team_id`,`ups`.`upped_id`;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
